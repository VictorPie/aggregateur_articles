import connexion_db as cd
import insert_delete as id
import password_hash as pw

req_creation_table_sources = """
                                CREATE TABLE IF NOT EXISTS t_sources (
                                    id_s SERIAL PRIMARY KEY,
                                    nm_source varchar(100),
                                    url_source varchar(256),
                                    url_flux varchar(256) UNIQUE NOT NULL,
                                    type_flux varchar(10)
                                )
                            """

req_creation_table_articles = """
                                CREATE TABLE IF NOT EXISTS t_articles (
                                    id_a SERIAL PRIMARY KEY,
                                    titre TEXT UNIQUE NOT NULL,
                                    date_publi DATE,
                                    description TEXT,
                                    url_article varchar(256) NOT NULL,
                                    date_entree DATE,
                                    id_source INTEGER REFERENCES t_sources(id_s)
                                );
                            """

req_creation_table_utilisateurs = """
                                CREATE TABLE IF NOT EXISTS t_utilisateurs (
                                    id_u SERIAL PRIMARY KEY,
                                    log_u TEXT UNIQUE NOT NULL,
                                    mdp_u varchar(256) NOT NULL,
                                    email_u TEXT UNIQUE NOT NULL,
                                    statut_u varchar(5)
                                );
                            """

req_creation_table_utilisateurs_sources = """
                                CREATE TABLE IF NOT EXISTS t_utilisateurs_sources (
                                    id_u INTEGER REFERENCES t_utilisateurs(id_u),
                                    id_s INTEGER REFERENCES t_sources(id_s)
                                );
                            """

req_creation_table_utilisateurs_articles = """
                                CREATE TABLE IF NOT EXISTS t_utilisateurs_articles (
                                    id_u INTEGER REFERENCES t_utilisateurs(id_u),
                                    id_a INTEGER REFERENCES t_articles(id_a),
                                    lu BOOLEAN
                                );
                            """

def creation_tables(conn):
    try:
        cur = conn.cursor()
        cur.execute(req_creation_table_sources)
        cur.execute(req_creation_table_articles)
        cur.execute(req_creation_table_utilisateurs)
        cur.execute(req_creation_table_utilisateurs_sources)
        cur.execute(req_creation_table_utilisateurs_articles)
    except cd.psycopg2.OperationalError as oe:
        print(oe)
    except cd.psycopg2.ProgrammingError as pe:
        print(pe)

mdp = pw.hash_password("admin")

user_test = {"log_u" : "Dominique" , "mdp_u" : mdp, "email_u" : "yo@yo.com", "statut_u" : "admin"}
user_test1 = {"log_u" : "Alice" , "mdp_u" : mdp, "email_u" : "yo1@yo.com", "statut_u" : "admin"}
user_test2 = {"log_u" : "Kevin" , "mdp_u" : mdp, "email_u" : "yo2@yo.com", "statut_u" : "admin"}
user_test3 = {"log_u" : "Victor" , "mdp_u" : mdp, "email_u" : "yo3@yo.com", "statut_u" : "admin"}

if __name__ == "__main__":
    with cd.conn_db() as conn:
        creation_tables(conn)
        id.insert_utilisateur(conn, user_test)
        id.insert_utilisateur(conn, user_test1)
        id.insert_utilisateur(conn, user_test2)
        id.insert_utilisateur(conn, user_test3)

