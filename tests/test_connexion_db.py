import sys
import pytest

# Manip pour permettre l'import d'un module se trouvant dans le répertoire parent
sys.path.append("..")

import connexion_db as cd
import psycopg2 as p2


def test_connexion():
    with cd.conn_db() as conn:
        # Teste si une variable est une instance d'une classe
        assert isinstance(conn, p2.extensions.connection)

        assert conn.closed == 0
