import sys
import pytest

sys.path.append("..")

import connexion_db as cd
import creation_tables as ct
import psycopg2 as p2


@pytest.fixture(scope="session")
def conn():
    with cd.conn_db() as conn:
        yield conn


def test_creation_tables(conn):
    ct.creation_tables(conn)
    cur = conn.cursor()
    cur.execute("SELECT COUNT(*) FROM t_sources")
    assert cur.fetchone()[0] == 0
    cur.execute("SELECT COUNT(*) FROM t_articles")
    assert cur.fetchone()[0] == 0


def test_structure_table_sources(conn):
    cur = conn.cursor()
    req = """
            select column_name,data_type 
            from information_schema.columns 
            where table_name = %s;
        """
    cur.execute(req,('t_sources',))

    resultat = cur.fetchall()
    assert resultat[0] == ('id_s','integer')
    assert resultat[1] == ('nm_source','character varying')
    assert resultat[2] == ('url_source','character varying')
    assert resultat[3] == ('url_flux','character varying')
    assert resultat[4] == ('type_flux','character varying') 


def test_structure_table_articles(conn):
    cur = conn.cursor()
    req = """
            select column_name,data_type
            from information_schema.columns
            where table_name = %s;
        """
    cur.execute(req,('t_articles',))

    resultat = cur.fetchall()
    assert resultat[0] == ('id_a','integer')
    assert resultat[1] == ('titre','text')
    assert resultat[2] == ('date_publi','date')
    assert resultat[3] == ('description','text')
    assert resultat[4] == ('url_article','character varying')
    assert resultat[5] == ('date_entree','date')
    assert resultat[6] == ('id_source','integer')


